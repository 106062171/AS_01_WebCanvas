# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

    這次的作業是要實作一個以網頁為基礎的小畫家，為了讓它能夠具有不同的功能(滑鼠左鍵點擊時，有不同的效果)，
我首先在 canvas.js 裡宣告了一個變數 currentMethod(canvas.js: 2) 來記錄這一次滑鼠點擊應該展現的效果(比如 brush, 
eraser... 等等)，接著我宣告了三個 eventlistener 分別監控 mousedown(canvas.js: 18), mousemove(canvas.js: 82) 和 
mouseup(canvas.js: 153) 三個事件，當這三個事件被觸發，就接著用前述的 currentMethod 來決定應該執行哪些動作。

## 以下分別描述各個功能的實作

1. brush</br>
		首先當 mousedown(canvas.js: 22) 時，先將 brush 的 color, size 跟據 index.html 表單的資料設定好，接著在
	使用者鼠標的當前位置畫一個點，而當 mousemove 時，用 ctx.lineTo 將上一個點和現在的點連起來，重複數次，直到 
	mouseup 為止。
2. eraser</br>
		eraser 的實作方法與brush非常相似，只需要將 color 固定成白色即可達到像皮擦的效果。
3. rect</br>
		rect在 mousedown(canvas.js: 38) 時，會先將使用者的當前滑鼠座標記錄下來，並在 mousemove(canvas.js: 96) 時，
	根據當前座標和先前記下來的座標用 ctx.rect 畫出一個長方形邊框，要注意的是每次畫出長方形邊樞之前，必須先將前一
	個長方形刪除再畫。
4. rectblock</br>
		rectblock 和 rect 非常相似差別在於 rectblock 是一個實心的長方形。
5. circle</br>
		和前面的 rect 的概念相同，只是畫線的函數是 ctx.arc(canvas.js: 113) ，而記錄的點則為圓心。
6. circleblock</br>
		circleblock 的實作方法則是先用和 circle 相同的方法先畫出一個圓框，再用 ctx.fill(canvas.js: 121) 將 circle 
	填滿。
7. triangle</br>
		triangle 用來畫出一個等腰三角形，用和前面幾個功能相同的想法實作，而第一個記錄的點則為三角形頂點， mousemove
	(canvas.js: 132) 所拿到的座標則為另一個頂點，再根據這兩個點，推算出第三個點。
8. triangleblock</br>
		triangleblock 用和 circleblock 同樣的想法，先用 circle 畫出一個圓框，再用 ctx.fill(canvas.js: 140) 將這個三
	角形填滿。
9. line</br>
		line 則是用一開始記錄的點，和 mousemove(canvas.js: 142) 後所拿到的座標連成一條線。
10. reset(canvas.js: 158)</br>
		reset 利用 ctx.clearRect 將整張畫布清空。
11. undo(canvas.js: 162)</br>
		undo 的實作方法為每當我做了一個動作(即一個 mousedown 事件)，就先將當前還沒更改的畫布 push 到 undo_buffer 裡。
	當使用者按下 undo 按扭，再從 undo_buffer pop 原來的畫布。
12. redo(canvas.js: 169)</br>
		redo 的實作方法也和 undo 相似，當使用者按下 undo 按扭時，便將當前尚未複原的畫布 push 到 redo_buffer 裡。
	當使用者按下 redo 按扭時，再從 redo_buffer pop 這張畫布。值得注意的是，每當使用者輸入任何一個指令時，必需將 
	redo_list 清空。
